#core/server/models.py

from core.server import db

class Song(db.Model):

    __tablename__ = "song"

    id = db.Column(db.Integer, primary_key=True,unique=True,autoincrement=True,nullable=False)
    title = db.Column(db.String(64), nullable=False)
    year = db.Column(db.Integer)


    def __init__(self, title, year):
        self.title = title
        self.year = year
    
    