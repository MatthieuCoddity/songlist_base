#core/server/app.py
from flask import render_template, request, make_response, jsonify
from core.server import app, db
from core.server.models import Song

@app.route('/add', methods=['POST'])
def add():
    """ Add a song """
    data = request.get_json()
    try:
        title = Song.query.filter_by(title = data.get('title')).first()
        if title:
            response = {
                'status' : 'fail',
                'message' : 'song already in base'
            }
            return make_response(jsonify(response)),409
        else:
            song = Song(title = data.get('title'), year = data.get('year'))
            db.session.add(song)
            db.session.commit()
            response = {
                'status' : 'success',
                'message' : 'song added'
            }
            return make_response(jsonify(response)),200
    except Exception as e:
        response = {
            'status' : 'fail',
            'message' : 'error'
        }
        return make_response(jsonify(response)),500

@app.route('/getlist',methods=['GET'])
def getlist():
    """ Get the whole list of songs """
    try:
        songs = Song.query.all()
        response = []
        for i in songs:
            song = dict()
            song['title'] = i.title
            song['year'] = i.year
            response.append(song)
        if response :
            return make_response(jsonify(response)),200
        else:
            response = {
                'status' : 'fail',
                'message' : 'songlist is empty'   
            }
            return make_response(jsonify(response)),409
    except Exception as e:
        response = {
            'status' : 'fail',
            'message' : 'error'
        }
        return make_response(jsonify(response)),500

@app.route('/clear',methods=['DELETE'])
def clear():
    """ Clear the list """
    try:
        Song.query.delete()
        db.session.commit()
        response = {
            'status' : 'success',
            'message' : 'all list cleared'
        }  
        return make_response(jsonify(response)),200
    except Exception as e:
        response = {
            'status' : 'fail',
            'message' : 'error'
        }
        return make_response(jsonify(response)),500
    return
