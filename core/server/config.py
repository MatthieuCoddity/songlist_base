#core/server/config.py


import os
basedir = os.path.abspath(os.path.dirname(__file__))

#database configuration
psql_base = os.getenv('DATABASE_URI')
psql_base_test = os.getenv('DATABASE_URI_TEST')

class BaseConfig:
    """Base configuration."""
    #secret key env variable, 'sweet_secret' default
    SECRET_KEY = os.getenv('SECRET_KEY', 'mon_secret')
    #debug
    DEBUG = False
    #DB
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class TestingConfig(BaseConfig):
    """Testing configuration."""
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = psql_base_test
    PRESERVE_CONTEXT_ON_EXCEPTION = False

class DevelopmentConfig(BaseConfig):
    """Development configuration."""
    #debug
    DEBUG = True
    #DB
    SQLALCHEMY_DATABASE_URI = psql_base

class ProductionConfig(BaseConfig):
    """Production configuration."""
    SECRET_KEY = os.getenv('SECRET_KEY', 'mon_secret')
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = psql_base
