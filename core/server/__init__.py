#core/__init__.py
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

#app creation
app = Flask(__name__)

#looking for env variable APP_SETTINGS, core.server.config.DevelopementConfig by default
app_settings = os.getenv(
    'APP_SETTINGS',
    'core.server.config.DevelopmentConfig'
)
app.config.from_object(app_settings)

#database object
db = SQLAlchemy(app)

from core.server.app import *